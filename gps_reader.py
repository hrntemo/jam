import serial
import threading
from pynmea2 import parse

class GPSReader:
    def __init__(self, port, baud_rate):
        self.port = port
        self.baud_rate = baud_rate
        self.serial = None
        self.is_running = False
        self.thread = None
        self.fix = False
        self.lat = None
        self.lon = None
        self.alt = None
        self.satellites = None
        self.hdop = None
        self.satellite_data = []
        self.hdop_data = []

    def start(self):
        self.serial = serial.Serial(self.port, self.baud_rate)
        self.is_running = True
        self.thread = threading.Thread(target=self.read_data)
        self.thread.start()

    def stop(self):
        self.is_running = False
        if self.thread is not None:
            self.thread.join()
        if self.serial is not None:
            self.serial.close()
    def read_data(self):
        while self.is_running:
            try:
                data = self.serial.readline().decode("utf-8").strip()
                if data.startswith(("$GPGGA", "$GAGGA", "$GBGGA", "$GLGGA", "$GNGGA")):
                    msg = parse(data)
                    self.parse_gga(msg)
            except:
                pass
    def parse_gga(self, msg):
        self.fix = msg.gps_qual > 0
        self.lat = msg.latitude
        self.lon = msg.longitude
        self.alt = msg.altitude
        self.satellites = msg.num_sats
        self.hdop = msg.horizontal_dil
                # Uydu ve HDOP verilerini listelere ekleyin
        self.satellite_data.append(self.satellites)
        self.hdop_data.append(self.hdop)
            # En son 100 veriyi saklamak için sınırlama uygulayın
        self.satellite_data = self.satellite_data[-100:]
        self.hdop_data = self.hdop_data[-100:]
