from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QComboBox, QPushButton, QGridLayout, QFrame, QGroupBox,QHBoxLayout
from PyQt5.QtCore import QTimer
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import serial.tools.list_ports
import csv
from PyQt5.QtWidgets import QFileDialog
from gps_reader import GPSReader
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from PyQt5.QtWebEngineWidgets import QWebEngineView
import folium
import io
import base64
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QUrl
import os

class GPSApp(QWidget):
    def __init__(self):
        super().__init__()
        self.save_continuous = False
        self.gps_reader = None
        self.positions = []
        self.map = None
        self.init_ui()
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_data)
        self.timer.start(3000)  # 10 saniyede bir güncelle

    def init_ui(self):
        layout = QVBoxLayout()

        self.setup_gps_connection()
        self.setup_gps_data_plot()
        self.setup_gps_data_display()

        layout.addWidget(self.connection_groupbox)
        layout.addWidget(self.plot_groupbox)
        layout.addWidget(self.data_groupbox)

        self.setLayout(layout)

    def setup_gps_connection(self):
        self.connection_groupbox = QGroupBox("GPS Bağlantısı")
        self.connection_groupbox.setFixedHeight(100)        
        layout = QGridLayout()

        self.port_combobox = QComboBox()
        self.port_combobox.addItems([str(port) for port in serial.tools.list_ports.comports()])
        port = QLabel("Port:")

        self.refresh_button = QPushButton("Yenile")
        self.refresh_button.clicked.connect(self.refresh_ports)

        self.baud_combobox = QComboBox()
        self.baud_combobox.addItems(["4800", "9600", "19200", "38400", "57600", "115200"])
        baud = QLabel("Baud Oranı:")

        self.connect_button = QPushButton("Bağlan")
        self.connect_button.clicked.connect(self.connect_gps)
        self.disconnect_button = QPushButton("Durdur")
        self.disconnect_button.clicked.connect(self.disconnect_gps)

        layout.addWidget(port, 0, 0)
        layout.addWidget(self.port_combobox, 0, 1)
        layout.addWidget(self.refresh_button,0,2)
        layout.addWidget(baud, 0, 3)
        layout.addWidget(self.baud_combobox, 0, 4)
        layout.addWidget(self.connect_button, 0, 5)
        layout.addWidget(self.disconnect_button ,0, 6)

        self.connection_groupbox.setLayout(layout)

    def setup_gps_data_plot(self):
        self.plot_groupbox = QGroupBox("GPS Veri Grafiği")
        layout = QVBoxLayout()

        self.fig = make_subplots(specs=[[{"secondary_y": True}]])
        self.fig.add_trace(go.Scatter(x=[], y=[], mode="lines+markers", name="Uydu Sayısı"), secondary_y=False)
        self.fig.add_trace(go.Scatter(x=[], y=[], mode="lines+markers", name="HDOP"), secondary_y=True)
        self.fig.update_yaxes(title_text="Uydu Sayısı", secondary_y=False)
        self.fig.update_yaxes(title_text="HDOP", secondary_y=True)

        self.web_engine_view = QWebEngineView()
        self.web_engine_view.setHtml(self.fig.to_html(include_plotlyjs="cdn", full_html=False))
        layout.addWidget(self.web_engine_view)

        self.plot_groupbox.setLayout(layout)

    def setup_gps_data_display(self):
        self.data_groupbox = QGroupBox("GPS Veri")
        self.data_groupbox.setFixedHeight(100)
        layout = QGridLayout()

        self.fix_label = QLabel()
        self.satellite_label = QLabel()
        self.lat_label = QLabel()
        self.lon_label = QLabel()
        self.alt_label = QLabel()
        self.hdop_label = QLabel()
        layout.addWidget(QLabel("GPS Fix:"),0,0)
        layout.addWidget(self.fix_label,0,1)
        layout.addWidget(QLabel("Uydu Sayısı:"),0,2)
        layout.addWidget(self.satellite_label,0,3)
        layout.addWidget(QLabel("Enlem:"),0,4)
        layout.addWidget(self.lat_label,0,5)
        layout.addWidget(QLabel("Boylam:"),0,6)
        layout.addWidget(self.lon_label,0,7)
        layout.addWidget(QLabel("Yükseklik:"),0,8)
        layout.addWidget(self.alt_label,0,9)
        layout.addWidget(QLabel("HDOP:"),0,10)
        layout.addWidget(self.hdop_label,0,11)
        # "Kaydet" düğmesini oluşturun
        self.save_button = QPushButton("Kaydet")
        self.save_button.clicked.connect(self.save_data)
        layout.addWidget(self.save_button,0,12)

        self.map_button = QPushButton("Haritada Göster")
        self.map_button.clicked.connect(self.show_on_map)
        layout.addWidget(self.map_button,0,13)
        self.data_groupbox.setLayout(layout)

    def refresh_ports(self):
        self.port_combobox.clear()
        self.port_combobox.addItems([str(port.device) for port in serial.tools.list_ports.comports()])

    def connect_gps(self):
        try:
            if self.gps_reader is not None:
                self.gps_reader.stop()

            port = self.port_combobox.currentText()
            baud_rate = int(self.baud_combobox.currentText())
            self.gps_reader = GPSReader(port, baud_rate)
            self.gps_reader.start()
        except Exception as e:
            self.show_error_message(str(e))

    def show_error_message(self, message):
        error_message = QMessageBox()
        error_message.setIcon(QMessageBox.Critical)
        error_message.setWindowTitle("Hata")
        error_message.setText("Bir hata oluştu:")
        error_message.setInformativeText(message)
        error_message.exec_()


    def update_data(self):
        if self.gps_reader is None:
            return

        self.fix_label.setText("Evet" if self.gps_reader.fix else "Hayır")
        self.satellite_label.setText(str(self.gps_reader.satellites))
        self.lat_label.setText(str(self.gps_reader.lat))
        self.lon_label.setText(str(self.gps_reader.lon))
        self.alt_label.setText(str(self.gps_reader.alt))
        self.hdop_label.setText(str(self.gps_reader.hdop))

        self.update_plot()
        if hasattr(self, "map_view"):
            self.show_on_map()
        if self.save_continuous:
            self.save_data()

    def update_plot(self):
        # Veri güncellemesi
        x_data = list(range(1, len(self.gps_reader.satellite_data) + 1))
        self.fig.update_traces(x=x_data, y=self.gps_reader.satellite_data, selector=dict(name="Uydu Sayısı"))
        self.fig.update_traces(x=x_data, y=self.gps_reader.hdop_data, selector=dict(name="HDOP"))

        # Grafiği güncelle
        self.web_engine_view.setHtml(self.fig.to_html(include_plotlyjs="cdn", full_html=False))

    def disconnect_gps(self):
        if self.gps_reader is not None:
            self.gps_reader.stop()

    def save_data(self):
        if self.gps_reader is None or not self.gps_reader.fix:
            return
        if not self.save_continuous:
            filename = QFileDialog.getSaveFileName(self, "GPS Verilerini Kaydet", "", "CSV Dosyaları (*.csv)")[0]

            if filename:
                self.save_continuous = True
                self.save_filename = filename
                with open(self.save_filename, "w") as file:
                    writer = csv.writer(file)
                    writer.writerow(["GPS Fix", "Uydu Sayısı", "Enlem", "Boylam", "Yükseklik", "HDOP"])
        else:
            with open(self.save_filename, "a") as file:
                writer = csv.writer(file)
                writer.writerow([
                    "Evet" if self.gps_reader.fix else "Hayır",
                    self.gps_reader.satellites,
                    self.gps_reader.lat,
                    self.gps_reader.lon,
                    self.gps_reader.alt,
                    self.gps_reader.hdop
                ])
            self.save_continuous = False
            self.save_button.setText("Kaydet")
        

        filename = QFileDialog.getSaveFileName(self, "GPS Verilerini Kaydet", "", "CSV Dosyaları (*.csv)")[0]

        if filename:
            with open(filename, "w") as file:
                writer = csv.writer(file)
                writer.writerow(["GPS Fix", "Uydu Sayısı", "Enlem", "Boylam", "Yükseklik", "HDOP"])
                writer.writerow([
                    "Evet" if self.gps_reader.fix else "Hayır",
                    self.gps_reader.satellites,
                    self.gps_reader.lat,
                    self.gps_reader.lon,
                    self.gps_reader.alt,
                    self.gps_reader.hdop
                ])

    #Harita Gösterme Fonksiyonu 
    def show_on_map(self):
        if self.gps_reader is None or not self.gps_reader.fix:
            return

        self.positions.append([self.gps_reader.lat, self.gps_reader.lon])

        self.map = folium.Map(location=self.positions[-1], zoom_start=13)

        folium.PolyLine(self.positions, color="blue", weight=2.5, opacity=1).add_to(self.map)
        folium.Marker(location=self.positions[-1]).add_to(self.map)

        map_html = self.map.get_root().render()

        # HTML dosyasını geçici olarak kaydedin
        with open("map.html", "w") as f:
            f.write(map_html)

        # QWebEngineView ile haritayı gösterin
        if not hasattr(self, "map_view"):
            self.map_view = QWebEngineView()

        self.map_view.load(QUrl.fromLocalFile(os.path.abspath("map.html")))
        self.map_view.show()
