import sys
from PyQt5.QtWidgets import QApplication

from gps_ui import GPSApp

def main():
    app = QApplication(sys.argv)

    with open("style/style.qss", "r") as f:
        app.setStyleSheet(f.read())

    window = GPSApp()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
